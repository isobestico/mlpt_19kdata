# mlpt_19kdata

   This is a repository to share some of the AIMD train/test configurations 
used in the work:

B. Herzog, M. Chagas da Silva, B. Casier, M. Badawi, F. Pascale, T. Bucko, S. Lebegue, D. Rocca; 
"Assessing the Accuracy of Machine Learning Thermodynamic Perturbation Theory: Density 
Functional Theory and Beyond" (2022); 
arXiv:2110.06818 [cond-mat.mtrl-sci]

https://arxiv.org/abs/2110.06818

# Data Organization

  The data were organized as follow

  - 5 reference functionals data (PBE, PBE+D2, vdW-DF2, SCAN and SCAN+rVV10)
  - 6 systems per functional (CH4, CO2, HCHAB, SiCHAB, CH4@HCHAB, and CO2@SiCHAB)
  - 5 data files per systems ( aimd_all_e0.txt, mann_kendall_trend.out, sample-19k.xyz, train.idx, test.idx)

# Description of Data

  The meaning/oring of the data per system in each functional is given bellow:

  * **aimd_all_e0.txt:** potential energy of the whole AIMD configurations including the equilibration (~200k entries)
  * **mann_kendall_trend.out:** it is the output of a Mann-Kendall trend analysis used to determine the equilibration period and the main reference averages
  * **sample-19.xyz:** it is a extended XYZ file containing the atoms, coodinates, forces, energy and unit cell information for a sample extracted evenly separated from the whole AIMD configurations.
  * **train/test.idx:** are the indeces related to the sample-19k.xyz configurations used to select the train and test 
dataset for the machinel learning protocols.

# VASP template

  The INCAR files per functionals used to run the single point 
and the dynamics are given in the folder 

  - incar_files

# Additional details information

  For requesting more information or additional data please contact the 
corresponde authors addressed at :

https://arxiv.org/abs/2110.06818
 
